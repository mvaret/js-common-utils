# JS COMMON UTILS #

* Collection of js functions

### Installation ###

```
yarn add https://mvaret@bitbucket.org/mvaret/js-common-utils.git
```

### List of functions ###

* intersect(array, array)
* leftPad(string, length, char)
* makeCancelable(promise)
* removeLastChar(string)