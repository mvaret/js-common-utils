module.exports = {
  // Returns intersection of a and b arrays
  intersect: (a, b) => {
    let t
    if(b.length > a.length)
      t = b, b = a, a = t

    return a
      .filter((e) => {
        if(b.indexOf(e) !== -1)
          return true
        })
      .filter((e, i, c) => c.indexOf(e) === i)
  },
  // Pad str with char to reach len
  leftPad: (str, len, char) => {
    return Array(len + 1).join(char).substring(0, len - str.length) + str
  },
  // Returns a promise that can be cancelled by calling cancel() function
  makeCancelable: (promise) => {
    let hasCanceled = false

    const wrappedPromise = new Promise((resolve, reject) => {
      promise.then((val) =>
        hasCanceled ? reject({isCanceled: true}) : resolve(val)
      )
      promise.catch((error) =>
        hasCanceled ? reject({isCanceled: true}) : resolve(error)
      )
    })

    return {
      promise: wrappedPromise,
      cancel() {
        hasCanceled = true
      },
    }
  },
  // Remove last char of string
  removeLastChar: (string) => {
	   return string.substring(0, string.length - 1)
  },
  sha256: (str) => {
    return require('crypto').createHash('sha256').update(str).digest('hex')
  }
}
